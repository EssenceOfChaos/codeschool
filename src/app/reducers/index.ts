import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer,
  on,
  createReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';

import { SIGN_IN, SUBMIT_QUIZ, UPDATE_PROFILE } from '../actions';
import { state } from '@angular/animations';

export interface State {
  // currentUser: string;
}
export const initialState: State = {
  currentUser: ''
};

export const reducers: ActionReducerMap<State> = {
  //
};

export const metaReducers: MetaReducer<State>[] = !environment.production
  ? []
  : [];
