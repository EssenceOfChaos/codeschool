import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { QuizRoutingModule } from "./quiz-routing.module";
import { QuizComponent } from "./quiz.component";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { QuizService } from "src/app/services/quiz.service";

// import material design module
import { MaterialModule } from "src/app/modules/material/material.module";

// import { MatRadioModule } from '@angular/material/radio';
// import { MatButtonModule } from '@angular/material/button';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatIconModule } from '@angular/material/icon';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatCardModule } from '@angular/material/card';

@NgModule({
  imports: [
    CommonModule,
    QuizRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    // MatRadioModule,
    // MatButtonModule,
    // MatProgressSpinnerModule,
    // MatIconModule,
    // MatInputModule,
    // MatCardModule,
    // MatFormFieldModule,
    MaterialModule
  ],
  declarations: [QuizComponent],
  providers: [QuizService]
})
export class QuizModule {}
