import { Component, OnInit } from "@angular/core";
import { Title } from "@angular/platform-browser";
import { QuizService } from "src/app/services/quiz.service";
import { Observable, of } from "rxjs";
import { ELIXIR_ANSWERS, JAVASCRIPT_ANSWERS, ANGULAR_ANSWERS } from "./answers";

import { Router, ActivatedRoute } from "@angular/router";
// import { switchMap } from 'rxjs/operators';

interface Question {
  id: number;
  question: string;
  choices: string[];
}

@Component({
  selector: "app-quiz",
  templateUrl: "./quiz.component.html",
  styleUrls: ["./quiz.component.css"]
})
export class QuizComponent implements OnInit {
  pageTitle = "Quiz";
  questions: Array<Question>;
  question: Observable<Question>;
  answers;
  // answers = ANSWERS;
  index = 0;
  isAnswered = false;
  quizFinished = false;
  correctAnswers = 0;
  userName: string;
  response;
  subject;

  // state of user's choice ('unanswered', 'no', 'yes')
  correctAnswer = "unanswered";
  currentUser = "";

  constructor(
    private title: Title,
    private quizService: QuizService,
    private route: ActivatedRoute,
    public router: Router
  ) {}

  ngOnInit() {
    console.log("ngOnInit Fired from QuizComponent.");
    this.title.setTitle(this.pageTitle);
    this.userName = history.state.data.currentUser;
    this.subject = this.route.snapshot.paramMap.get("subject");

    this.quizService.getQuestions(this.subject).subscribe(res => {
      if (res) {
        console.log(res);
        this.questions = res.questions;
      }
    });

    const subjects = {
      js: JAVASCRIPT_ANSWERS,
      elixir: ELIXIR_ANSWERS,
      angular: ANGULAR_ANSWERS
    };

    this.answers = subjects[this.subject];
  }

  nextQuestion(i: number) {
    if (i >= this.questions.length) {
      i = 0;
    }
    this.question = of(this.questions[i]);
    this.index++;
    this.isAnswered = false;
    this.correctAnswer = "unanswered";
  }

  submitChoice(response) {
    console.log(this.index, response);
    this.checkAnswer(response);
    this.isAnswered = true;
    if (this.index === this.questions.length - 1) {
      this.quizFinished = true;
      this.recordScore(this.userName, this.correctAnswers, this.subject);
      this.router.navigate(["/leaderboard"]);
    }
  }

  checkAnswer(choice) {
    const answer = this.answers[this.index].choice;
    if (choice === answer) {
      this.correctAnswer = "yes";
      this.correctAnswers = this.correctAnswers + 1;
    } else {
      this.correctAnswer = "no";
    }
  }

  recordScore(user, score, subject) {
    this.quizService.addScore(user, score, subject).subscribe(res => {
      console.log("Score Successfully Recorded");
    });
  }
}
