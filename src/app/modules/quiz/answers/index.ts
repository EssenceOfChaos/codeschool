import { ELIXIR_ANSWERS } from './elixir';
import { JAVASCRIPT_ANSWERS } from './javascript';
import { ANGULAR_ANSWERS } from './angular';

export { ELIXIR_ANSWERS, JAVASCRIPT_ANSWERS, ANGULAR_ANSWERS };
