import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

export interface Subject {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public currentUser = '';

  subjects: Subject[] = [
    { value: 'js', viewValue: 'JavaScript' },
    { value: 'elixir', viewValue: 'Elixir' },
    { value: 'angular', viewValue: 'Angular' }
  ];

  userForm = new FormGroup({
    name: new FormControl(''),
    subject: new FormControl('')
  });
  constructor(public router: Router) {}

  ngOnInit() {}

  startQuiz(formValues) {
    console.log(formValues);
    this.currentUser = formValues.name;
    this.router.navigate([`/quiz/${formValues.subject}`], {
      state: { data: { currentUser: formValues.name } }
    });
  }
}
