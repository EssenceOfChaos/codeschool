import { TestBed } from '@angular/core/testing';

import { LeaderboardService } from './leaderboard.service';
import { HttpClientModule } from '@angular/common/http';
// Http testing module and mocking controller
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('LeaderboardService', () => {
  let lbService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LeaderboardService],
      imports: [HttpClientModule, HttpClientTestingModule]
    });
    lbService = TestBed.get(LeaderboardService);
  });

  it('should be created', () => {
    const service: LeaderboardService = TestBed.get(LeaderboardService);
    expect(service).toBeTruthy();
  });
});
