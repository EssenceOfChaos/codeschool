import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { LeaderboardService } from './leaderboard.service';
import { MatTableDataSource, MatSort } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';

export interface UserInfo {
  user: string;
  score: string;
  subject: string;
  date: Date;
}
@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.css']
})
export class LeaderboardComponent implements OnInit, AfterViewInit {
  rank = 0;

  displayedColumns: string[] = ['user', 'score', 'quiz', 'date'];
  // dataSource = new MatTableDataSource(ELEMENT_DATA);
  // dataSource is fetched from the lb service
  // dataSource needs to be type of MatTableDataSource for sorting and pagination to work
  dataSource;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private lb: LeaderboardService) {}

  ngOnInit() {
    this.lb.getScores().subscribe(res => {
      if (res) {
        console.log(res);
        this.dataSource = res;
      }
    });
  }

  ngAfterViewInit() {
    // this.dataSource.sort = this.sort;
    // this.dataSource.paginator = this.paginator;
  }
}
