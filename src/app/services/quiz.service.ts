import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Quiz } from 'src/app/modules/quiz/quiz';

const quizPath = {
  angular: 'assets/data/angular_quiz.json',
  elixir: 'assets/data/elixir_quiz.json',
  js: 'assets/data/javascript_quiz.json'
};

@Injectable()
export class QuizService {
  quizUrl = 'assets/data/quiz.json';
  endpoint = environment.quizUrl;
  constructor(private http: HttpClient) {}

  getQuestions(subject): Observable<any> {
    const url = quizPath[subject];
    console.log(url);

    return this.http.get(url).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  addScore(user, score, subject) {
    const date = Date.now();
    const result: Quiz = {
      user,
      subject,
      date,
      score
    };
    console.log(result);
    return this.http
      .post<Quiz>(`${this.endpoint}/quizzes`, result)
      .pipe(catchError(this.handleError));
  }

  getQuizzes() {
    return this.http.get(`${this.endpoint}/quizzes`);
  }

  // generic error handler
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    return throwError('Something went wrong; please try again later.');
  }
}
