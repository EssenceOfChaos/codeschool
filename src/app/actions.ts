import { createAction } from '@ngrx/store';

export const SIGN_IN = createAction('[APPLICATION] SIGN_IN');
export const SUBMIT_QUIZ = createAction('[APPLICATION] SUBMIT_QUIZ');
export const UPDATE_PROFILE = createAction('[APPLICATION] UPDATE_PROFILE');
