import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';
import { HomeComponent } from './home/home.component';
import { SubjectsComponent } from './subjects/subjects.component';

const routes: Routes = [
  {
    path: 'quiz',
    loadChildren: () =>
      import('./modules/quiz/quiz.module').then(m => m.QuizModule)
  },
  { path: 'leaderboard', component: LeaderboardComponent },
  { path: 'subjects', component: SubjectsComponent },
  { path: '**', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
